const DataSource = require('./datasources.json');

//  Injeta Variaveis do ambiente
DataSource.db.url = process.env.DATABASE_URL;
DataSource.Email.transports[0].apikey = process.env.SENDGRID_API_KEY;
DataSource.storage.root = '/storage';

module.exports = DataSource;
