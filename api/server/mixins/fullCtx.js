'use strict'

/**
 * FullCtx
 */
module.exports = (Model) => {
  Model.createOptionsFromRemotingContext = function (fullCtx) {
    return Object.assign(
      this.base.createOptionsFromRemotingContext(fullCtx),
      { fullCtx }
    )
  }
}
