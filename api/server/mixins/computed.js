'use strict'

const debug = require('debug')('loopback:mixin:computed')
const _ = require('lodash')
const Promise = require('bluebird')

module.exports = (Model, options) => {
  //  Add the Full Context to the options, so the Computed property can access
  Model.createOptionsFromRemotingContext = function (ctx) {
    const base = this.base.createOptionsFromRemotingContext(ctx)
    return Object.assign(base, {
      fullCtx: ctx
    })
  }

  // Trigger a warning and remove the property from the watchlist when one of
  // the property is not found on the model or the defined callback is not found
  _.mapKeys(options.properties, (callback, property) => {
    if (_.isUndefined(Model.definition.properties[property])) {
      debug('Property %s on %s is undefined', property, Model.modelName)
    }

    if (typeof Model[callback] !== 'function') {
      debug('Callback %s for %s is not a model function', callback, property)
    }
  })

  debug('Computed mixin for Model %s with options %o', Model.modelName, options)

  // The loaded observer is triggered when an item is loaded
  Model.observe('loaded', (ctx, next) => {
    // We don't act on new instances
    if (ctx.isNewInstance) {
      return next()
    }
    return Promise.map(Object.keys(options.properties), property => {
      const callback = options.properties[property]

      if (typeof Model[callback] !== 'function') {
        debug('Function %s not found on Model', callback)
        return false
      }

      debug('Computing property %s with callback %s', property, callback)

      //  Verify if is using filter field, then don't call the function if not necessary
      if (!ctx.options.fullCtx) return false
      const ReqQuery = ctx.options.fullCtx.req.query
      if (ReqQuery.filter && JSON.parse(ReqQuery.filter).fields) {
        const fields = JSON.parse(ReqQuery.filter).fields
        if (fields.indexOf(property) === -1) return false
      }

      // `Promise.resolve` will normalize promises and raw values
      return Promise
        .resolve(Model[callback](ctx.data, ctx.options))
        .then(value => (ctx.data[property] = value))
    })
  })

  // The loaded observer is triggered when an item is loaded
  Model.observe('before save', (ctx, next) => {
    Object.keys(options.properties || {}).forEach(property => {
      debug('Removing computed property %s', property)

      if (ctx.instance) {
        ctx.instance.unsetAttribute(property)
      }
      if (ctx.data) {
        delete ctx.data[property]
      }
    })

    return next()
  })
}
