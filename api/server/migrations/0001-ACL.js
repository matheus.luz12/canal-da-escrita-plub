'use strict';
const debug = require('debug')('migration:0001-ACL');

module.exports = {
  up: async (app, callback) => {
    const { Role } = app.models;

    debug('Criando tabelas necessárias');
    await app.dataSources.db.autoupdate(['Role', 'RoleMapping']);

    debug('Limpando tabelas');
    const query = require('../../common/functions/query')(app.dataSources.db);
    await query('TRUNCATE TABLE roleMapping');
    await query('TRUNCATE TABLE role');

    await Role.create([
      { name: 'root', description: 'Desenvolvedores' },
      { name: 'admin', description: 'Gestor' },
      { name: 'teacher', description: 'Professor' },
    ]);

    callback();
  },

  down: (app, callback) => { callback(); },
};
