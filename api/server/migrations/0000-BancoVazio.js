'use strict'
const _ = require('lodash')
const debug = require('debug')('migrations:0000-BancoVazio')
const fs = require('fs')
const modelConfig = require('../model-config.json')
const path = require('path')

module.exports = {
  up: async (app, callback) => {
    debug('Start')

    debug('Atualizando tabelas', AutoUpdateTables)
    await app.dataSources.db.autoupdate(AutoUpdateTables)

    debug('Seed no banco de dados')
    const lstSeed = fs.readdirSync(path.join(__dirname, './Seed'))
      .map(path.parse)
      .filter(Row => Row.ext.toLowerCase() === '.json')
    debug({ lstSeed })

    const lstPromise = lstSeed.map(objFile => {
      const lstInsert = require(path.join(__dirname, './Seed/', objFile.base))
      return app.models[objFile.name].create(lstInsert)
    })
    await Promise.all(lstPromise)

    debug('Sucesso!')
    callback()
  },
  down: (app, callback) => { callback() }
}

const AutoUpdateTables = Object.entries(modelConfig)
  .filter(El => _(El).get('[1].dataSource') === 'db')
  .filter(El => _(El).get('[1].options.ignoreMigrate') !== true)
  .map(El => El[0])
module.exports.AutoUpdateTables = AutoUpdateTables
