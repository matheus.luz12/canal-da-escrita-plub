'use strict';
require('dotenv').config();

console.log('NODE_ENV', process.env.NODE_ENV);

const app = require('loopback')();
const boot = require('loopback-boot');

const { AutoUpdateTables } = require('./migrations/0000-BancoVazio');

boot(app, __dirname, async function (err) {
  if (err) throw err;
  console.log('Inicializando o migration');

  try {
    delete app.models.Migration.definition.settings.indexes;
    delete app.models.MigrationMap.definition.settings.indexes;

    console.log('Criando tabela migrations (Caso não exista)');

    //  Para forçar a inicialização do banco de dado
    // await app.models.Usuario.findOne();

    await app.dataSources.db.autoupdate('Migration');

    console.log('Executando as migrations');
    await app.models.Migration.migrate('up');

    console.log('Atualizando as estruturas dos bancos de dados');
    await app.dataSources.db.autoupdate(AutoUpdateTables);

    console.log('Fim!');
  } catch (error) {
    console.error(error);
    process.exitCode = 1;
  }

  process.exit();
});

