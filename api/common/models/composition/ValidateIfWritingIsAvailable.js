'use strict';
const dayjs = require('dayjs')
const debug = require('debug')('Composition:ValidateIfWritingIsAvailable')
const isBetween = require('dayjs/plugin/isBetween')
const isToday = require('dayjs/plugin/isToday')

dayjs.extend(isToday)
dayjs.extend(isBetween)

module.exports = Composition => {
  /**
   * Retorna a aderência de uso do produto no cliente
   * @param {number} clientId Identificação do cliente
   */
  Composition.ValidateIfWritingIsAvailable = async (usuario) => {
    debug('Start')
    // const usuario = await Composition.app.models.Usuario.findById(userId);
    const negativeResult = {
      hasAvaliable: false,
      quantityCompositionAvaliable: 0,
      dateLimit: dayjs(new Date()).format('YYYY-MM-DD'),
    }
    if (!usuario.transaction) return negativeResult
    const { date_updated: purchaseDate, installments } = usuario.transaction;
    const periods = Array.from({ length: installments }, (value, key) => {
      const start = dayjs(purchaseDate).add(key, 'month').format('YYYY-MM-DD')
      const end = dayjs(start).add(1, 'month').subtract(1, 'day').format('YYYY-MM-DD')
      return { start, end }
    })
    const period = periods.reduce((previousValue, currentValue) => {
      const { start, end } = currentValue
      const isBetweenPeriods = dayjs(new Date()).isBetween(start, end, null, '[]') || dayjs(start).isToday() || dayjs(end).isToday()
      if (isBetweenPeriods) previousValue = currentValue
      return previousValue
    }, {})

    if (!period || !period.start || !period.end) return negativeResult
    const where = {
      usuariosId: usuario.id,
      and: [
        { createdAt: { gte: period.start } },
        { createdAt: { lte: period.end } },
      ],
    };
    const quantityCompositionWriting = await Composition.count(where)

    const quantityCompositionAvaliable = 4 - quantityCompositionWriting
    const hasAvaliable = quantityCompositionAvaliable > 0
    const result = {
      hasAvaliable: hasAvaliable,
      quantityCompositionAvaliable: hasAvaliable ? quantityCompositionAvaliable : 0,
      dateLimit: dayjs(period.end),
    }
    debug('>> result: ', JSON.stringify(result));
    return result;
  };
};
