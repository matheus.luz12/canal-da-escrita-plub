'use strict';
const debug = require('debug')('Composition:observeBeforeCreateComposition')
const createError = require('http-errors');

module.exports = Composition => {
  Composition.observe('before save', hook(Composition));
};

const hook = Composition => async (ctx) => {
  debug('Start')
  if (!ctx.isNewInstance) return
  const userId = ctx.options.accessToken.userId;
  const { avaliableWriting } = await Composition.app.models.Usuario.findById(userId);
  // const avaliableWriting = await Composition.ValidateIfWritingIsAvailable(userId);
  debug('>> avaliableWriting: ', JSON.stringify(avaliableWriting));
  if (!avaliableWriting.hasAvaliable) throw createError(400, 'Não há redações disponíveis.');
  return;
};
