'use strict';
const cep = require('cep-promise')

module.exports = Zipcode => {
  /**
   * Retorna a aderência de uso do produto no cliente
   * @param {number} clientId Identificação do cliente
   */
  Zipcode.GetAddressToZipcode = async (zipcode) => {
    return cep(zipcode);
  };
};
