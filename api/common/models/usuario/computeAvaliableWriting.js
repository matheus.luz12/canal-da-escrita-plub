'use strict'
const debug = require('debug')('Usuario:computeAvaliableWriting')

module.exports = Usuario => {
  Usuario.computeAvaliableWriting = async function (item) {
    debug('Start')
    const avaliableWriting = await Usuario.app.models.Composition.ValidateIfWritingIsAvailable(item)
    debug('>> avaliableWriting: ', JSON.stringify(avaliableWriting));
    return avaliableWriting
  }
}
