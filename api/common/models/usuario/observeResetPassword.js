'use strict';
const debug = require('debug')('Usuario:observeResetPassword');

module.exports = Usuario => {
  Usuario.beforeRemote('resetPassword', function (ctx, model, next) {
    debug('Start');
    ctx.req.body.origin = ctx.req.headers.origin;
    next();
  });
};
