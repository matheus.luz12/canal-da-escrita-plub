'use strict';
const _ = require('lodash');

module.exports = Usuario => {
  const objUsuario = require('../usuario.json');
  const disableAllExcept = [
    ..._(objUsuario).get('mixins.SetupRemoteMethods.disableAllExcept') || [],
    ..._(objUsuario.acls).map(Row => Row.property).flattenDeep(),
  ];

  lstMethodsDefault
    .filter(RM => !disableAllExcept.includes(RM))
    .forEach(RM => Usuario.disableRemoteMethodByName(RM));
};

/**
 *
 */
const lstMethodsDefault = [
  'changePassword',
  'confirm',
  'count',
  'create',
  'createChangeStream',
  'deleteById',
  'exists',
  'find',
  'findById',
  'findOne',
  'login',
  'logout',
  'patchOrCreate',
  'prototype.__count__accessTokens',
  'prototype.__create__accessTokens',
  'prototype.__delete__accessTokens',
  'prototype.__destroyById__accessTokens',
  'prototype.__findById__accessTokens',
  'prototype.__get__accessTokens',
  'prototype.__updateById__accessTokens',
  // 'prototype.patchAttributes',
  'prototype.verify',
  'replaceById',
  'replaceOrCreate',
  // 'resetPassword',
  'setPassword',
  'updateAll',
  'upsertWithWhere',
];
