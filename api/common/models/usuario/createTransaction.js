'use strict'
const _ = require('lodash')
const createError = require('http-errors');
const debug = require('debug')('Usuario:createTransaction')
const pagarme = require('pagarme')

module.exports = Usuario => {
  Usuario.prototype.createTransaction = async function (payment) {
    debug('Start')
    let client

    debug('Inicializando API do PAGAR.ME')
    try {
      client = await pagarme.client.connect({ api_key: process.env.PAGARME_API })
    } catch (err) {
      console.error(err)
      throw createError(400, 'Ocorreu um erro ao tentar se conectar com o PAGAR.ME');
    }

    debug('Criando transação')

    const instPlan = await Usuario.app.models.Plan.findById(payment.planId)

    const dataTransaction = getDataTransaction(this, payment, instPlan);


    const transaction = await client.transactions.create(dataTransaction)
    debug('transaction', transaction.status, transaction.status_reason)

    switch (transaction.status) {
      case 'refused':
        if (transaction.refuse_reason === 'antifraud') throw createError(400, 'Transação Recusada pelo Antifraude')
        throw createError(400, 'Transação recusada na autorização')

      case 'paid':
        debug('Persistindo dados da transação no usuário')
        await this.updateAttribute('transaction', {
          status: transaction.status,
          date_updated: new Date(transaction.date_updated),
          id: transaction.id,
          installments: transaction.installments,
        })
        break;

      default: throw createError(400, 'Situação não prevista')
    }
  }
}

function getDataTransaction (instUsuario, payment, instPlan) {
  const dataTransaction = {
    card_number: payment.cardNumber,
    card_cvv: payment.cardCvv,
    card_expiration_date: payment.cardExpirationDate,
    card_holder_name: payment.cardHolderName,
    installments: instPlan.installments,
    customer: {
      external_id: instUsuario.id.toString(),
      name: instUsuario.name,
      type: 'individual',
      country: 'br',
      email: instUsuario.email,
      documents: [{ type: 'cpf', number: instUsuario.documentNumber, }],
      phone_numbers: [`+55${instUsuario.phone.ddd}${instUsuario.phone.number}`],
      birthday: instUsuario.bornAt.toISOString().slice(0, 10),
    },
    billing: {
      name: payment.customer.name,
      address: {
        country: 'br',
        state: payment.customer.address.state,
        city: payment.customer.address.city,
        neighborhood: payment.customer.address.neighborhood,
        street: payment.customer.address.street,
        street_number: payment.customer.address.streetNumber,
        zipcode: payment.customer.address.zipcode,
      }
    },
    items: [{
      id: instPlan.id.toString(),
      title: instPlan.title,
      unit_price: instPlan.price * 100,
      quantity: 1,
      tangible: false
    }]
  };

  dataTransaction.amount = _(dataTransaction.items).map(Row => Row.unit_price * Row.quantity).reduce((a, b) => a + b, 0)

  return dataTransaction
}
