'use strict'
const debug = require('debug')('Usuario:computeIsPaid')
const dayjs = require('dayjs')

module.exports = Usuario => {
  Usuario.computeIsPaid = async (item) => {
    debug('Start')
    if (!item.transaction) return false

    const dataUltimoPagamento = dayjs(item.transaction.date_updated).format('YYYY-MM-DD')
    const isPagamentoEmDia = dayjs(dataUltimoPagamento).add(item.transaction.installments, 'month').isAfter()
    debug('isPagamentoEmDia', isPagamentoEmDia);

    return isPagamentoEmDia
  }
}
