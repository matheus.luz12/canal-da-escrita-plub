'use strict';
const debug = require('debug')('Usuario:resetPassword');

module.exports = function (Usuario) {
  Usuario.on('resetPasswordRequest', async (info) => {
    debug('Start');

    const body = bodyFormat(info);

    const message = {
      subject: '[Canal da Escrita] Redefinir a senha',
      body,
      email: info.email,
      usuariosId: info.accessToken.userId,
    };

    Usuario.app.models.Message.SendEmail(message);
  });
};

/**
 *
 * @param {*} info
 */
function bodyFormat (info) {
  const hostname = info.options.origin || 'https://aluno.canaldaescrita.com.br'
  const urlReset = new URL(`${hostname}/#/recover?access_token=${info.accessToken.id}`);

  debug('bodyFormat', { urlReset });

  const html = `
  <p>Olá,</p>
  <p>Clique neste link para redefinir a senha de login no Canal da Escrita com sua conta.</p>
  <p><a href="${urlReset}">${urlReset}</a></p>
  <p>Se você não solicitou a redefinição da sua senha, ignore este e-mail.</p>
  <p>Obrigado</p>`;

  const text = `Olá,
  Clique neste link para redefinir a senha de login no Canal da Escrita com sua conta. ${urlReset}
  Se você não solicitou a redefinição da sua senha, ignore este e-mail.
  Obrigado
  `
  return { text, html };
}

