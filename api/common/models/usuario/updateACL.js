'use strict';
const debug = require('debug')('Usuario:updateACL');

module.exports = Usuario => {
  /**
   * Método para atualizar as permissões do usuário
   * @param {number} id Identificador do usuário
   * @param {array} lstACL lista de permissões do usuário
   * @param {string} realm Nome da organização do usuário
   */
  Usuario.updateACL = async (id, lstACL) => {
    debug('Start')

    if (!lstACL.length) return

    const role = await Usuario.app.models.Role.find({ where: { name: lstACL }, fields: 'id' });

    await Usuario.app.models.RoleMapping.destroyAll({ principalId: id });

    if (!(role && role.length)) return;

    const upsertData = role.map(item => ({
      principalType: 'USER',
      principalId: id,
      roleId: item.id,
    }));

    await Usuario.app.models.RoleMapping.create(upsertData)
      .catch(Err => {
        throw new Error('Erro durante atualização de permissão!', Err);
      });

    return;
  };
};
