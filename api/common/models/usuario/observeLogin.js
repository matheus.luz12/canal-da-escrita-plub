'use strict';
const debug = require('debug')('Usuario:observeLogin');
const createError = require('http-errors');

module.exports = Usuario => {
  Usuario.afterRemote('login', hook(Usuario))
};

const hook = Usuario => async (ctx, instance) => {
  debug('Start');
  debug('host:', ctx.req.headers.host);
  const usuario = await Usuario.findById(instance.userId);
  if (usuario && !usuario.isEnable) throw createError(401, 'Usuário não está ativo.');
  const { lstACL, isPaid } = usuario
  if (lstACL && lstACL.length) return
  if (!isPaid) throw createError(401, 'Pagamento não está em dia.');
  return
};
