'use strict'
const _ = require('lodash')
const debug = require('debug')('Usuario:computeLstACL')

module.exports = Usuario => {
  Usuario.computeLstACL = async (item) => {
    debug('Start')
    if (!item.id) return undefined
    const query = require('../../functions/query')(Usuario.app.datasources.db)

    return _(await query(stmt, { principalId: item.id }))
      .map(El => El.name)
      .uniq()
      .sort()
      .filter(El => El)
      .value()
  }
}

const stmt = `SELECT R.name
FROM RoleMapping AS RM
INNER JOIN Role AS R ON RM.roleId = R.id
WHERE (RM.principalType = 'USER')
  AND (RM.principalId = @principalId)`
