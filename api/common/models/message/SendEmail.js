'use strict';
const path = require('path')
const debug = require('debug')('Message:SendEmail');
const sendgridMail = require('@sendgrid/mail');

module.exports = Message => {
  Message.SendEmail = async (messageData) => {
    debug('Start');

    const { from, apikey } = require(path.resolve(__dirname, '../../../server/datasources.json'))['Email'].transports[0];
    debug('apikey', apikey);

    sendgridMail.setApiKey(apikey);

    const { email, body, subject, usuariosId } = messageData;

    const messageCreated = await Message.create({ type: 0, email, subject, body: body.text, usuariosId });
    debug('messageCreated', messageCreated);

    const data = {
      to: email,
      from,
      subject,
      text: body.text,
      html: body.html,
    }

    try {
      if (process.env.NODE_ENV !== 'production') return
      await sendgridMail.send(data);
      debug('sendgridMail:sended');
      await messageCreated.updateAttribute('wasSent', true);
      return true;
    } catch (error) {
      debug(error);
      return false;
    }
  }
}
