const _ = require('lodash')
const { isCPF } = require('brazilian-values')
const createError = require('http-errors');
const debug = require('debug')('Payment:Subscribe')

module.exports = Payment => {
  Payment.Subscribe = async (user, payment) => {
    debug('Start')
    const instUsuario = await criarUsuario(Payment.app, user);
    try {
      await instUsuario.createTransaction(payment)
    } catch (error) {
      debug('Error Transaction')
      await Payment.app.models.Usuario.destroyById(instUsuario.id)
    }

    return 'OK'
  };
};

/**
 *
 * @param {*} app
 * @param {*} user
 */
async function criarUsuario (app, user) {
  const {
    Usuario
  } = app.models

  if (!isCPF(user.documentNumber)) {
    throw createError(400, 'CPF do usuário inválido');
  }

  const hasUsuario = await Usuario.count({
    documentNumber: user.documentNumber
  }).then(Boolean)
  if (hasUsuario) {
    throw createError(400, 'Usuário já possuí cadastro');
  }

  const instUsuario = new Usuario(user);
  instUsuario.bornAt = new Date(user.bornAt.split('/').reverse().join('-'));
  instUsuario.username = user.documentNumber;
  await instUsuario.save()

  return instUsuario;
}
