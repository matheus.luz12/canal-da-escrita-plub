'use strict'
const debug = require('debug')('Topic:computedSents')

module.exports = Topic => {
  Topic.computedSents = async (item) => {
    debug('Start')
    const result = await Topic.app.models.Composition.count({ topicId: item.id })
    debug('result', result)
    return result || 0;
  }
}

