'use strict'
const lodash = require('lodash')
const debug = require('debug')('Topic:computedAverage')

module.exports = Topic => {
  Topic.computedAverage = async (item) => {
    debug('Start')
    const compositions = await Topic.app.models.Composition.find({ where: { topicId: item.id, status: 'Corrigido' } });
    if (!compositions || !compositions.length) return 0;

    const test = lodash.reduce(compositions, (accumulator, currentItem) => {
      const competenceTotal = lodash.reduce(Object.values(currentItem.rating), (sum, rating) => sum + Number(rating.value), 0);
      return accumulator + competenceTotal / 5;
    }, 0);
    const average = Number(test / compositions.length).toFixed(0)
    debug('average', average)
    return average;
  }
}
