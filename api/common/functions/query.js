'use strict';
const debug = require('debug')('functions:query');
/**
 * Otimizações para o comando QueryExecute, que é muito fraquinho!
 * @param {string} stmt Comando Query a ser executado
 * @param {object} params Parâmetos do comando Query a ser executado
 * @param {Function(Error, object)} callback
 */

module.exports = db => function (stmt, params = {}, callback = () => { }) {
  debug('Start');
  debug('stmt', stmt);
  debug('params', params);

  const regexSQLParams = /\@([\w.$]+|"[^"]+"|'[^']+')/g;
  let index = 1;
  let args = stmt.match(regexSQLParams);
  if (args !== null) args = args.map(Field => params[Field.replace('@', '')]);
  debug('args', args);

  const sql = stmt.replace(regexSQLParams, (m, g, o, s) => (`$${index++}`));
  debug('sql', sql);

  return new Promise((resolve, reject) => {
    db.connector.execute(sql, args, (Err, Rows) => {
      if (Err) {
        reject(Err);
      } else {
        resolve(Rows);
      }

      debug('Err', Err);
      debug('Rows', Rows);
      callback(Err, Rows);
    });
  });
};
