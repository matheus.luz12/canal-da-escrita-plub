module.exports = function (ctx) {
  return {
    supportTS: false,
    boot: [
      'axios',
      'vue-async-data-2',
      'vuelidate',
      'vue-acl',
      'croppa',
      'el-tiptap'
    ],
    css: [
      'app.sass'
    ],
    extras: [
      'ionicons-v4',
      'mdi-v5',
      'fontawesome-v5',
      'eva-icons',
      'themify',
      'line-awesome',
      'roboto-font',
      'material-icons'
    ],
    build: {
      vueRouterMode: 'hash',
      distDir: './dist',
      publicPath: '/',
      showProgress: false,
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules/
        })
      },
      env: {
        API: ctx.dev ? '/api' : 'https://api.canaldaescrita.com.br/api',
        QUASAR_ENV: process.env.QUASAR_ENV
      }
    },
    devServer: {
      https: false,
      port: 4002,
      open: true,
      proxy: {
        '/api': {
          target: 'http://localhost:3000',
          changeOrigin: true,
          pathRewrite: {
            '^/api': '/api'
          }
        }
      }
    },
    framework: {
      iconSet: 'line-awesome',
      lang: 'pt-br',
      config: {},
      importStrategy: 'auto',
      plugins: ['Notify', 'Loading', 'Dialog']
    },
    animations: 'all',
    ssr: {
      pwa: false
    },
    pwa: {
      workboxPluginMode: 'GenerateSW',
      workboxOptions: {},
      manifest: {
        name: 'Canal da Escrita',
        short_name: 'Canal da Escrita',
        description: 'Plataforma Canal da Escrita',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            src: 'icons/icon-128x128.png',
            sizes: '128x128',
            type: 'image/png'
          },
          {
            src: 'icons/icon-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'icons/icon-256x256.png',
            sizes: '256x256',
            type: 'image/png'
          },
          {
            src: 'icons/icon-384x384.png',
            sizes: '384x384',
            type: 'image/png'
          },
          {
            src: 'icons/icon-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
      }
    },
    cordova: {},
    capacitor: {
      hideSplashscreen: true
    },
    electron: {
      bundler: 'packager',
      packager: {},
      builder: {
        appId: 'canal-da-escrita'
      },
      nodeIntegration: true
    }
  }
}
