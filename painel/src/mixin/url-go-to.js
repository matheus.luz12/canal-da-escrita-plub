export const UrlGoToMixin = {
  methods: {
    urlGoTo () {
      const urlGoTo = window.location.hash.substring(1)

      window.sessionStorage.setItem('urlGoTo', urlGoTo)

      if (urlGoTo.substr(-6).toLowerCase() === '/login') {
        delete window.sessionStorage.urlGoTo
      }

      if (window.sessionStorage.autoLogin) {
        this.$router.push({ path: '/auth/token', query: JSON.parse(window.sessionStorage.autoLogin) })
      }
    }
  }
}
