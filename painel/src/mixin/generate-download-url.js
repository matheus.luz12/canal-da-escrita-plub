import urlJoin from 'url-join'

export const GenerateDownloadURLMixin = {
  methods: {
    generateDownloadURL (container, file) {
      return urlJoin(this.$axios.defaults.baseURL, '/Storages/', container, '/download/', file)
    }
  }
}
