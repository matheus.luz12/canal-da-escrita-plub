
const routes = [
  {
    path: '/',
    redirect: '/compositions'
  },
  {
    path: '/profile',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isTeacher' },
    children: [
      {
        path: '/',
        name: 'profile',
        component: () => import('pages/profile.vue'),
        meta: { rule: 'isTeacher' }
      }
    ]
  },
  {
    path: '/compositions',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isTeacher' },
    children: [
      {
        path: '/',
        component: () => import('pages/compositions/compositions.vue'),
        meta: { rule: 'isTeacher' }
      }
    ]
  },
  {
    path: '/composing/:id',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isTeacher' },
    children: [
      {
        path: '/',
        component: () => import('pages/compositions/composing.vue'),
        meta: { rule: 'isTeacher' }
      }
    ]
  },
  {
    path: '/videos',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isAdmin' },
    children: [
      {
        path: '/',
        component: () => import('pages/videos.vue'),
        meta: { rule: 'isAdmin' }
      }
    ]
  },
  {
    path: '/topics',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isAdmin' },
    children: [
      {
        path: '/',
        component: () => import('pages/topics.vue'),
        meta: { rule: 'isAdmin' }
      }
    ]
  },
  {
    path: '/plans',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isAdmin' },
    children: [
      {
        path: '/',
        component: () => import('pages/plans.vue'),
        meta: { rule: 'isAdmin' }
      }
    ]
  },
  {
    path: '/categories',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isAdmin' },
    children: [
      {
        path: '/',
        component: () => import('pages/categories.vue'),
        meta: { rule: 'isAdmin' }
      }
    ]
  },
  {
    path: '/users',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isAdmin' },
    children: [
      {
        path: '/',
        component: () => import('pages/users.vue'),
        meta: { rule: 'isAdmin' }
      }
    ]
  },
  {
    path: '/recover',
    component: () => import('layouts/login.vue'),
    meta: { rule: 'isUnauthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/recover.vue'),
        meta: { rule: 'isUnauthenticated' }
      }
    ]
  },
  {
    path: '/auth/login',
    component: () => import('layouts/login.vue'),
    meta: { rule: 'isUnauthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/auth/login.vue'),
        meta: { rule: 'isUnauthenticated' }
      }
    ]
  },
  {
    path: '/auth/token',
    component: () => import('layouts/login.vue'),
    meta: { rule: 'isUnauthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/auth/token.vue'),
        meta: { rule: 'isUnauthenticated' }
      }
    ]
  },
  {
    path: '/auth/logout',
    component: () => import('layouts/login.vue'),
    meta: { rule: 'isUnauthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/auth/logout.vue'),
        meta: { rule: 'isUnauthenticated' }
      }
    ]
  },
  {
    path: '*',
    component: () => import('pages/error404.vue'),
    meta: { rule: 'isUnauthenticated' }
  }
]

export default routes
