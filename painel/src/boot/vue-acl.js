import { AclInstaller, AclCreate, AclRule } from 'vue-acl'

export default ({ router, Vue }) => {
  Vue.use(AclInstaller)

  const aclCreate = new AclCreate({
    initial: 'unauthenticated',
    notfound: {
      path: '/auth/login'
    },
    router,
    acceptLocalRules: true,
    globalRules: {
      isRoot: new AclRule('root').generate(),
      isAdmin: new AclRule('admin').or('root').generate(),
      isTeacher: new AclRule('teacher').or('admin').or('root').generate(),
      isAuthenticated: new AclRule('authenticated').or('teacher').or('admin').or('root').generate(),
      isUnauthenticated: new AclRule('unauthenticated').or('authenticated').or('teacher').or('admin').or('root').generate()
    }
  })

  Vue.use(aclCreate)
}
