export const ErrorMessagesMixin = {
  methods: {
    errorMessages ($v) {
      const messages = []

      if (!$v.$error) {
        return messages
      }

      Object.keys($v.$params).forEach((param) => {
        if ($v[param] === false) {
          switch (param) {
            case 'required':
              messages.push('Campo obrigatório')
              break

            case 'minLength':
              messages.push(`Mínimo ${$v.$params.minLength.min} caracteres`)
              break

            case 'maxLength':
              messages.push(`Máximo ${$v.$params.maxLength.max} caracteres`)
              break

            case 'minValue':
              messages.push('Informe um número válido')
              break

            case 'email':
              messages.push('Informe um e-mail válido')
              break

            case 'fullName':
              messages.push('Informe o nome completo')
              break

            case 'sameAsPassword':
              messages.push('Confirme sua senha')
              break
          }
        }
      })

      return messages
    }
  }
}
