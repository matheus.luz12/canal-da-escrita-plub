export const FetchUserMixin = {
  methods: {
    async fetchUser ({ userId, accessToken }, redirect = true) {
      if (!userId || !accessToken) {
        this.$router.push('/auth/login')

        return undefined
      }

      window.sessionStorage.setItem('autoLogin', JSON.stringify({ userId, accessToken }))
      this.$axios.defaults.headers.common.Authorization = accessToken

      try {
        const axiosConfig = {
          method: 'GET',
          url: `/Usuarios/${userId}`,
          params: { filter: { fields: ['id', 'name', 'email', 'transaction', 'avatar', 'avaliableWriting'] } }
        }

        const session = await this.$axios(axiosConfig).then(R => R.data)

        delete session.transaction

        this.$acl.change('authenticated')
        this.$store.commit('session/set', { user: session })

        if (redirect) {
          this.$router.push(window.sessionStorage.urlGoTo || '/')
        }
      } catch (err) {
        this.axiosCatch(err, '/auth/logout')
      }
    }
  }
}
