import { AclInstaller, AclCreate, AclRule } from 'vue-acl'

export default ({ router, Vue }) => {
  Vue.use(AclInstaller)

  const aclCreate = new AclCreate({
    initial: 'unauthenticated',
    notfound: {
      path: '/auth/login'
    },
    router,
    acceptLocalRules: true,
    globalRules: {
      isAdmin: new AclRule('admin').generate(),
      isAuthenticated: new AclRule('authenticated').or('admin').generate(),
      isUnauthenticated: new AclRule('unauthenticated').or('authenticated').or('admin').generate()
    }
  })

  Vue.use(aclCreate)
}
