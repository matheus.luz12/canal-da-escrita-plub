import ElementUI from 'element-ui'
import { ElementTiptapPlugin } from 'element-tiptap'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-tiptap/lib/index.css'

export default ({ Vue }) => {
  Vue.use(ElementUI)
  Vue.use(ElementTiptapPlugin, { lang: 'en', spellcheck: true })
}
