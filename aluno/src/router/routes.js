
const routes = [
  {
    path: '/',
    redirect: '/compositions'
  },
  {
    path: '/profile',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isAuthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/user/profile.vue'),
        meta: { rule: 'isAuthenticated' }
      }
    ]
  },
  {
    path: '/compositions',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isAuthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/compositions/compositions.vue'),
        meta: { rule: 'isAuthenticated' }
      }
    ]
  },
  {
    path: '/composing/:id',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isAuthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/compositions/composing.vue'),
        meta: { rule: 'isAuthenticated' }
      }
    ]
  },
  {
    path: '/videos',
    component: () => import('layouts/main.vue'),
    meta: { rule: 'isAuthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/lessons/videos.vue'),
        meta: { rule: 'isAuthenticated' }
      }
    ]
  },
  {
    path: '/recover',
    component: () => import('layouts/login.vue'),
    meta: { rule: 'isUnauthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/user/recover.vue'),
        meta: { rule: 'isUnauthenticated' }
      }
    ]
  },
  {
    path: '/auth/login',
    component: () => import('layouts/login.vue'),
    meta: { rule: 'isUnauthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/auth/login.vue'),
        meta: { rule: 'isUnauthenticated' }
      }
    ]
  },
  {
    path: '/auth/token',
    component: () => import('layouts/login.vue'),
    meta: { rule: 'isUnauthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/auth/token.vue'),
        meta: { rule: 'isUnauthenticated' }
      }
    ]
  },
  {
    path: '/auth/logout',
    component: () => import('layouts/login.vue'),
    meta: { rule: 'isUnauthenticated' },
    children: [
      {
        path: '/',
        component: () => import('pages/auth/logout.vue'),
        meta: { rule: 'isUnauthenticated' }
      }
    ]
  },
  {
    path: '*',
    component: () => import('pages/error404.vue'),
    meta: { rule: 'isUnauthenticated' }
  }
]

export default routes
