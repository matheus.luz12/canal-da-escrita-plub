const parseDbUri = require("parse-db-uri")

module.exports = ({ env }) => {
  const datasource = parseDbUri(env('DATABASE_URL', 'postgres://root:root@0.0.0.0:5432/website'))

  return {
    defaultConnection: 'default',
    connections: {
      default: {
        connector: 'bookshelf',
        settings: {
          client: datasource.protocol,
          host: datasource.resource,
          port: datasource.port,
          database: datasource.database,
          username: datasource.user,
          password: datasource.password,
        },
        options: {}
      },
    },
  }
};
