#!/bin/bash
clear

echo
echo 'Clonando o GIT'
rm -rf ./www
git clone dokku@191.252.177.59:www
cd ./www
rm -rf ./css ./fonts ./icons ./img ./js
rm *
cd ..

echo
echo 'Buildando o Quasar'
rm -rf ./dist
npm run build
mv ./dist/* ./www/
touch > ./www/.static

echo
echo 'Efetuando Deploy'
cd ./www
git add --all
git commit -am "Deploy"
git push --force
cd ..