export function resetState (state) {
  Object.assign(state, require('./state.js').default)
}

export function set (state, session) {
  Object.assign(state, session)
}
