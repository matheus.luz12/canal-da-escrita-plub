import VueSimpleMarkdown from 'vue-simple-markdown'
// You need a specific loader for CSS files like https://github.com/webpack/css-loader
import 'vue-simple-markdown/dist/vue-simple-markdown.css'

export default async ({ Vue }) => {
  Vue.use(VueSimpleMarkdown)
}
