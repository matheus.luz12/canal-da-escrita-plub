
const routes = [
  {
    path: '/',
    component: () => import('layouts/main.vue'),
    children: [
      {
        path: '',
        name: 'main',
        component: () => import('pages/main.vue')
      },
      {
        path: '/terms',
        name: 'terms',
        component: () => import('pages/terms.vue')
      },
      {
        path: '/policy',
        name: 'policy',
        component: () => import('pages/policy.vue')
      }
    ]
  },
  {
    path: '/checkout',
    component: () => import('layouts/checkout.vue'),
    children: [
      {
        path: '',
        name: 'checkout',
        component: () => import('pages/checkout.vue')
      }
    ]
  },
  {
    path: '*',
    component: () => import('pages/error404.vue')
  }
]

export default routes
