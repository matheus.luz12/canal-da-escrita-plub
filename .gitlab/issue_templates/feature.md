### User Story:

**Eu como** (Aluno/Professor/Admin)

**Quero**
<!--
Descreva o que essa pessoa quer ou precisa.
Isso ajuda a sugerir alternativas caso a demanda não seja viável.

Exemplo: Quero poder emitir um relatório do movimento financeiro do dia.
-->

**Porque**
<!--
Qual é a finalidade dessa demanda, o por que o cliente realmente precisa disso.
Isso ajuda a definir a prioridade e pertinência da demanda.

Exemplo: Porque quero poder comparar com o valor físico presente no caixa
-->

-----

### Critérios de aceitação
<!-- O que o cliente espera receber para concluir o issue. -->

* [ ]  Somente usuário autenticado possuí acesso a esta rotina

### Demais observações
<!-- Documentos, exemplos e demais anexos/informações... -->