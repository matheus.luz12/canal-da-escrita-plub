### Resumo
<!-- Versão bem resumida do problema -->

### Como simular a inconsistência?
<!-- Com qual cliente? Passo a passo para simular o erro -->

### Qual é o comportamento inconsistente?
<!-- O que o sistema esta fazendo -->

### Qual é o comportamento esperado?
<!-- O que o sistema deveria fazer -->

### Screenshot/Logs relevantes
<!-- O screenshot do erro auxilia a identificar o problema. -->

/label ~"To Do" ~bug