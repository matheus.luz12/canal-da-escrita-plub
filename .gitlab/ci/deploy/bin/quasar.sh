#!/bin/sh

QUASAR_FOLDER=$1
cd /gitlab/matheus.luz12/canal-da-escrita/${QUASAR_FOLDER}

echo Instalando Packages
sudo npm ci

echo Buildando o Quasar
sudo npm run build

echo Preparando repositorio do app para Deploy
rm -rf /tmp/dokku-monorepo/${QUASAR_FOLDER}
mkdir -p /tmp/dokku-monorepo/
cd /tmp/dokku-monorepo/
git clone dokku@127.0.0.1:${QUASAR_FOLDER}
rm -rf /tmp/dokku-monorepo/${QUASAR_FOLDER}/*

echo Preparando arquivos
cp -R /gitlab/matheus.luz12/canal-da-escrita/${QUASAR_FOLDER}/dist/* /tmp/dokku-monorepo/${QUASAR_FOLDER}/

echo Enviando arquivos para o app
cd /tmp/dokku-monorepo/${QUASAR_FOLDER}/
git add --all
git commit -am "Deploy" || true
git push
